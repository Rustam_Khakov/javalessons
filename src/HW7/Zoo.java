package HW7;

/**
 * @author Rustam Khakov
 */
public class Zoo {
    public static void main(String[] args) {
        Animal[] animals = new Animal[4];
        animals[0] = new Cat("Барсик");
        animals[1] = new Cow("Буренка");
        animals[2] = new Dog("Шарик");
        animals[3] = new Sheep("Шон");
        for (int i = 0; i < animals.length; i++) {
            animals[i].voice();
            System.out.println(animals[i]);
            System.out.println(animals[i].isVegetarian());
        }
    }
}
