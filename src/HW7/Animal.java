package HW7;

/**
 * @author Rustam Khakov
 */
public class Animal {
    protected final String name;

    public Animal(String name) {
        this.name = name;
    }

    public void voice(){
        System.out.println("голос");
    }

    public boolean isVegetarian(){
        return false;
    }

    @Override
    public String toString() {
        return "Animals{" +
                "name='" + name + '\'' +
                '}';
    }

}
