package HW7;

/**
 * @author Rustam Khakov
 */
public class Cow extends Animal {
    public Cow(String name) {
        super(name);
    }

    @Override
    public boolean isVegetarian() {
        return true;
    }

    @Override
    public void voice() {
        System.out.println("MYYY");

    }
}
