package HW7;

/**
 * @author Rustam Khakov
 */
public class Cat extends Animal {

    public Cat(String name) {
        super(name);
    }

    @Override
    public boolean isVegetarian() {
        return false;
    }

    @Override
    public void voice() {
        System.out.println("Мяу");
    }

    @Override
    public String toString() {
        return "Cat{}" + name;
    }
}