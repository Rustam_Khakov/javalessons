package HW7;

/**
 * @author Rustam Khakov
 */
public class Dog extends Animal {
    public Dog(String name) {
        super(name);
    }

    @Override
    public boolean isVegetarian() {
        return false;
    }

    @Override
    public void voice() {
        System.out.println("GAV");

    }
}
