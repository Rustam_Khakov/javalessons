package HW7;

/**
 * @author Rustam Khakov
 */
public class Sheep extends Animal {
    public Sheep(String name) {
        super(name);
    }

    @Override
    public boolean isVegetarian() {
        return true;
    }

    @Override
    public void voice() {
        System.out.println("БЕЕ");
    }
}
