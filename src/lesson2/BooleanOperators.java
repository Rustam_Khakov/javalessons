package lesson2;

/**
 * @author Rustam Khakov
 */
public class BooleanOperators {
    public static void main(String[] args) {
        boolean first = (10 % 2) == 1;
        int x = 10;
        boolean isOdd = x%2 ==0;// делится на 2 без остатка
        boolean isDividedForThree = x%3==0; // делится на 3 без остатка
        boolean res = isOdd && isDividedForThree; // делится на 2 и на 3 без остатка
        System.out.println("число " + x + " делится ли на 2 и на 3? " + res);

        boolean res2 = isOdd || isDividedForThree; // делится на 2 или на 3 без остатка
        System.out.println("число " + x + " делится ли на 2 или на 3? " + res2);

    }
}
