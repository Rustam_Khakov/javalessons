package lesson2;

/**
 * @author Rustam Khakov
 */
public class MultiplyMatrix {
    public static void main(String[] args) {
        for (int i = 1; i < 10; i++) {
            for (int j = 1; j < 10; j++) {
                System.out.println(i + "x" + j + "=" + i * j);
            }
            System.out.println("================");
        }


        for (int i = 0; i < 10; i++) {
            if (i % 2 == 0 && i % 3 == 0) {
                System.out.println(// %d - double  %f - float %s - строка
                        String.format("%d делится на 2 и на 3 ",i));
            }
        }

    }
}
