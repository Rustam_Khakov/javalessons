package lesson2;

/**
 * @author Rustam Khakov
 */
public class IfExample {
    public static void main(String[] args) {
        int x = 6;
        if (x % 2 == 0) {
            //операция
            System.out.println("четное");
        } else {
            System.out.println("не четное");
        }
        int a = 1;
        int b = 2;
        int c = 3;
        if (a == b && b == c && c == a) { //если <> то
            System.out.println("Равносторонний");
        } else if (a == b || b == c || c == a) { // иначе если <> то
            System.out.println("Равнобедренный");
        } else {
            System.out.println("обычный треугольник");
        }

        int val = 3;
        if (val == 0) {
            System.out.println("ноль");
        } else if (val == 1) {
            System.out.println("один");
        } else if (val == 2) {
            System.out.println("два");
        }
        switch (val) {
            case 0:
                System.out.println("ноль");
                break;
            case 1:
                System.out.println("один");
                break;
            default:
                System.out.println("не 1 и не 0");
        }
    }
}
