package game;

import java.util.Random;

/**
 * @author Rustam Khakov
 */
public class BlackJack {
    private Deck deck = new Deck();
    private Player[] players = new Player[4];
    private Dealer dealer;
    private Player winner;
    private int gamersCount;

    public BlackJack() {
        players[0] = new Dealer("Диллер");
        gamersCount++;
    }

    public void addPlayerToGame(Player player) {
        players[gamersCount] = player;
        gamersCount++;
    }

    public void dealCardsToPlayers() {
        for (int i = 0; i < gamersCount; i++) {
            players[i].addCardToHand(deck.getRandomCard());
            players[i].addCardToHand(deck.getRandomCard());
        }
    }

    public void dealMainRound() {
        for (int i = 0; i < gamersCount; i++) {
            System.out.println("Ход " + players[i]);
            while (players[i].needMoreCards()) {
                players[i].addCardToHand(deck.getRandomCard());
            }
        }
    }

    public void printWinner() {
        Player playerMax = null;
        for (int i = 0; i < gamersCount; i++) {
            if (players[i].getSize() > 21) {
                System.out.println(players[i] + " проиграл");
                continue;
            }
            if (playerMax == null ||
                    players[i].getSize() > playerMax.getSize()) {
                playerMax = players[i];
            }
        }
        if (playerMax != null) {
            for (int i = 0; i < gamersCount; i++) {
                if (playerMax.getSize() == players[i].getSize()) {
                    System.out.println(players[i] + " победитель");
                    System.out.println(players[i].getSize());
                    players[i].openCards();
                }
            }
        } else {
            System.out.println("у всех перебор");
        }
    }
}
