package game;

/**
 * @author Rustam Khakov
 */
public enum Card {
    ONE_TREF(1, "один треф"),
    ONE_BUBI(1, "один буби"),
    ONE_PICI(1, "один пики"),
    ONE_CHERV(1, "один черв"),
    TWO_TREF(2, "два треф"),
    TWO_BUBI(2, "два буби"),
    TWO_PICI(2, "два пики"),
    TWO_CHERV(2, "два черв"),
    THREE_TREF(3, "три треф"),
    THREE_BUBI(3, "три буби"),
    THREE_PICI(3, "три пики"),
    THREE_CHERV(3, "три черв"),
    FOUR_TREF(4, "четыре треф"),
    FOUR_BUBI(4, "четыре буби"),
    FOUR_PICI(4, "четыре пики"),
    FOUR_CHERV(4, "четыре черв"),
    FIVE_TREF(5, "пять треф"),
    FIVE_BUBI(5, "пять буби"),
    FIVE_PICI(5, "пять пики"),
    FIVE_CHERV(5, "пять черв"),
    SIX_TREF(6, "шесть треф"),
    SIX_BUBI(6, "шесть буби"),
    SIX_PICI(6, "шесть пики"),
    SIX_CHERV(6, "шесть черв"),
    SEVEN_TREF(7, "семь треф"),
    SEVEN_BUBI(7, "семь буби"),
    SEVEN_PICI(7, "семь пики"),
    SEVEN_CHERV(7, "семь черв"),
    EIGTH_TREF(8, "восемь треф"),
    EIGTH_BUBI(8, "восемь буби"),
    EIGTH_PICI(8, "восемь пики"),
    EIGTH_CHERV(8, "восемь черв"),
    NINE_TREF(9, "девять треф"),
    NINE_BUBI(9, "девять буби"),
    NINE_PICI(9, "девять пики"),
    NINE_CHERV(9, "девять черв"),
    TEN_TREF(10, "десять треф"),
    TEN_BUBI(10, "десять буби"),
    TEN_PICI(10, "десять пики"),
    TEN_CHERV(10, "десять черв"),
    VALET_TREF(10, "валет треф"),
    VALET_BUBI(10, "валет буби"),
    VALET_PICI(10, "валет пики"),
    VALET_CHERV(10, "валет черв"),
    QUIEEN_TREF(10, "дама треф"),
    QUIEEN_BUBI(10, "дама буби"),
    QUIEEN_PICI(10, "дама пики"),
    QUIEEN_CHERV(10, "дама черв"),
    KINQ_TREF(10, "король треф"),
    KINQ_BUBI(10, "король буби"),
    KINQ_PICI(10, "король пики"),
    KINQ_CHERV(10, "король черв"),
    TUZ_TREF(11, "туз треф"),
    TUZ_BUBI(11, "туз буби"),
    TUZ_PICI(11, "туз пики"),
    TUZ_CHERV(11, "туз черв");



    private int value;
    private String name;

    Card(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public void open() {
        System.out.println(this);
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }
}
