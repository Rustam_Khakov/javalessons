package game;

/**
 * @author Rustam Khakov
 */
public interface Player {
    void addCardToHand(Card card);
    boolean needMoreCards();
    void openCards();
    int getSize();

}
