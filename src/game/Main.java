package game;

/**
 * @author Rustam Khakov
 */
public class Main {
    public static void main(String[] args) {
        BlackJack game = new BlackJack();
        game.addPlayerToGame(new GamePlayer("первый игрок"));
        game.addPlayerToGame(new GamePlayer( "игрок Джон"));

        game.dealCardsToPlayers();
        game.dealMainRound();
        game.printWinner();
    }
}
