package game;

import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Rustam Khakov
 */
public class Deck {
    public Card getRandomCard() {
        int size = Card.values().length;
        int val = ThreadLocalRandom.current().nextInt(size);
        return Card.values()[val];
    }
}
