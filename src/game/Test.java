package game;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Rustam Khakov
 */
public class Test {

    public static void main(String[] args) {

        Map<Integer, String> names = new HashMap<>();
        names.put(1, "ONE_");
        names.put(2, "TWO_");
        names.put(3, "THREE_");
        names.put(4, "FOUR_");
        names.put(5, "FIVE_");
        names.put(6, "SIX_");
        names.put(7, "SEVEN_");
        names.put(8, "EIGTH_");
        names.put(9, "NINE_");
        names.put(10, "TEN_");
        names.put(11, "VALET_");
        names.put(12, "QUIEEN_");
        names.put(13, "KINQ_");
        names.put(14, "TUZ_");



        Map<Integer, String> namesRus = new HashMap<>();
        namesRus.put(1, "один");
        namesRus.put(2, "два");
        namesRus.put(3, "три");
        namesRus.put(4, "четыре");
        namesRus.put(5, "пять");
        namesRus.put(6, "шесть");
        namesRus.put(7, "семь");
        namesRus.put(8, "восемь");
        namesRus.put(9, "девять");
        namesRus.put(10, "десять");
        namesRus.put(11, "валет");
        namesRus.put(12, "дама");
        namesRus.put(13, "король");
        namesRus.put(14, "туз");
        for (int i = 1; i < 15; i++) {
            int num = i> 10? 10: i;
            System.out.println(names.get(i) + "TREF("+ num +", \"" + namesRus.get(i) + " треф\"),");
            System.out.println(names.get(i) + "BUBI("+ num +", \"" + namesRus.get(i) + " буби\"),");
            System.out.println(names.get(i) + "PICI("+ num +", \"" + namesRus.get(i) + " пики\"),");
            System.out.println(names.get(i) + "CHERV("+ num +", \"" + namesRus.get(i) + " черв\"),");

        }
    }
}
