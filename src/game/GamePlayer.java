package game;

import java.util.Scanner;

/**
 * @author Rustam Khakov
 */
public class GamePlayer implements Player {
    private Card[] cards = new Card[21];
    private int size;
    private String name;
    private int currentCardsNumber;

    public GamePlayer(String name) {
        this.name = name;
    }

    @Override
    public void addCardToHand(Card card) {
        cards[currentCardsNumber] = card;
        size += card.getValue();
        currentCardsNumber++;
    }

    @Override
    public boolean needMoreCards() {
        openCards();
        System.out.println(getSize());
        System.out.println("--------------");
        System.out.println("нужно еще карты? ");
        Scanner scanner = new Scanner(System.in);
        String next = scanner.next();
        if (next.equals("да")) {
            return true;
        }
        return false;
    }

    @Override
    public void openCards() {
        for (int i = 0; i < currentCardsNumber; i++) {
            cards[i].open();
        }
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public String toString() {
        return name;
    }
}
