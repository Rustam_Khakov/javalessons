package game;

/**
 * @author Rustam Khakov
 */
public class Dealer extends GamePlayer {
    public Dealer(String name) {
        super(name);
    }

    @Override
    public boolean needMoreCards() {
        if (getSize() > 16) {
            return false;
        }
        return true;
    }
}
