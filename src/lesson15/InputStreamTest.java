package lesson15;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.RandomAccessFile;
import java.io.Reader;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;

/**
 * @author Rustam Khakov
 */
public class InputStreamTest {
    public static void main(String[] args) {
        // copyFile("test.txt", "test2.txt");

//        try (Reader reader = new FileReader("test.txt");){
//            while (reader.ready()) {
//                System.out.println(reader.read());
//            }
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        //testRandomAccess();
        Player player = new Player(15, "Vlad");
        writeObj(player);
    }

    public static void writeObj(Object obj) {
        try (ObjectOutputStream os =
                     new ObjectOutputStream(new FileOutputStream("object.txt"))) {
            os.writeObject(obj);
           // new File("test/txt.txt").mkdirs();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (ObjectInputStream is =
                     new ObjectInputStream(new FileInputStream("object.txt"))) {
            Object o = is.readObject();
            System.out.println(o instanceof Player);
            System.out.println(o);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void testRandomAccess() {
        try (RandomAccessFile file =
                     new RandomAccessFile("test.txt", "rw")) {
            file.seek(1_000);
            String s = file.readLine();
            System.out.println(s);
            file.writeBytes("Hello");
            FileChannel channel = file.getChannel();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void copyFile(String in, String out) {
        try (InputStream is = new BufferedInputStream(
                new FileInputStream(in));
             OutputStream os = new BufferedOutputStream(
                     new FileOutputStream(out)
             );
        ) {
            long start = System.currentTimeMillis();
            while (is.available() > 0) {
                os.write(is.read());
            }
            long stop = System.currentTimeMillis();
            System.out.println(stop - start);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void test() {
        String str = "Привет Мир";
        byte[] inBytes = str.getBytes(StandardCharsets.UTF_8);
        byte[] bytes = new byte[inBytes.length];
        try (InputStream in = new ByteArrayInputStream(inBytes)) {
            while (in.available() > 0) {
                in.read(bytes);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        String res = new String(bytes, StandardCharsets.UTF_8);
        System.out.println(res);
    }
}
