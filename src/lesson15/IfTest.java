package lesson15;

import java.util.Random;

/**
 * @author Rustam Khakov
 */
public class IfTest {

    public static void main(String[] args) {
        Random random = new Random();
        boolean isTrue = random.nextBoolean();
        String result = isTrue ? "Орел" : "Решка";
        System.out.println(result);
    }
}
