package lesson15;

/**
 * @author Rustam Khakov
 */
public class PasswordChecker {

    public static boolean validate(String password) {
        if (password == null) {
            return false;//throw new ...Exception();
        }
        boolean hasUpperCase = false;
        boolean hasDigits = false;
        boolean hasLowerCase = false;
        boolean length = password.length() >8;
        for ( int i =0; i< password.length();i++) {
            char c = password.charAt(i);
            hasDigits = hasDigits || Character.isDigit(c);
            hasUpperCase = hasUpperCase || Character.isUpperCase(c);
            hasLowerCase = hasLowerCase || Character.isLowerCase(c);
        }
        return hasDigits && hasLowerCase&& hasUpperCase && length;
    }
}
