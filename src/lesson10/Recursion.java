package lesson10;

/**
 * @author Rustam Khakov
 */
public class Recursion {

    public static void main(String[] args) {
        int[] array = new int[10];
        array[0] = 3;
        array[1] = 4;
        array[2] = 2;
        array[3] = 39;
        array[4] = 38;
        array[5] = 36;
        array[6] = 30;
        array[7] = 83;
        array[8] = 38;
        array[9] = 37;

        getMax(array, Integer.MIN_VALUE, 0);
    }

    private static void getMax(int[] array, int max, int i) {
        //точка остановки
        if (i == array.length) {
            System.out.println(max);
            return;
        }
        ///логика основная
        if(array[i] > max) {
            max = array[i];
        }
        //условие изменения
        getMax(array, max, i+1);
    }
}
