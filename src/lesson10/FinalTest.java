package lesson10;

/**
 * @author Rustam Khakov
 */
public class FinalTest {
    final static double PI = 3.14;

    public static void main(String[] args) {
        String example = "example string";
        // "example string"
        String result = example + " second";
        //"example string"  "example string second"
        StringBuilder builder = new StringBuilder();
        builder.append("example");
        //"example"
        if (true) {
            builder.append(" second");
            //"example second"
        }
        builder.toString();
    }
}
