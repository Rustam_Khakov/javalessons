package lesson10;

/**
 * @author Rustam Khakov
 */
public class HomeWork10 {
    public static void main(String[] args) {
        int[] array = new int[10];
        array[0] = 3;
        array[1] = 4;
        array[2] = 2;
        array[3] = 39;
        array[4] = 38;
        array[5] = 36;
        array[6] = 30;
        array[7] = 83;
        array[8] = 38;
        array[9] = 37;

        System.out.println(fib(10));
        System.out.println(calcSum(array, 0, 0));
        System.out.println(calcSum2(array, 0));
        System.out.println(minValue(array, Integer.MAX_VALUE, 0));


    }

    /**
     * метод реализующий подсчет числа Фибоначии
     *
     * @param number - порядковый номер в последовательности Фибоначи
     * @return возвращает значение
     */
    public static int fib(int number) {
        // F(n) = 0 -> 0
        if (number == 0) {
            return 0;
        }
        // F(n) = 1 -> 1
        if (number == 1) {
            return 1;
        }
        // F(n) = F(n-1) + F(n-2)
        return 0;//тут написать
    }

    /**
     *
     * метод для подсчета суммы чисел в массиве
     * @param array  массив
     * @param sum - текущая сумма
     * @param index индекс элемента
     * @return возвращает сумму
     */
    public static int calcSum(int[] array, int sum, int index){
        return 0;//todo реализуй меня
    }

    /**
     *
     * метод для подсчета суммы чисел в массиве без сумматора
     * @param array  массив
     * @param index индекс элемента
     * @return возвращает сумму
     */
    public static int calcSum2(int[] array, int index){
        //текущий плюс сумма следующих
        return 0;//todo реализуй меня
    }

    /**
     *
     * метод для подсчета суммы чисел в массиве без сумматора
     * @param array  массив
     * @param min - текущее минимальное значение
     * @param index индекс элемента
     * @return возвращает минимальное значение в массиве
     */
    public static int minValue(int[] array, int min, int index){
        return min;//todo реализуй меня
    }
}
