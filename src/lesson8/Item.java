package lesson8;

import java.util.Objects;

/**
 * @author Rustam Khakov
 */
public class Item {
    protected int price;
    protected String name;

    public Item(int price, String name) {
        this.price = price;
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return price == item.price &&
                Objects.equals(name, item.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(price, name);
    }
}
