package lesson8;

import java.util.Objects;

/**
 * @author Rustam Khakov
 */
public class FoodItem extends Item {
    boolean isHealthy;

    public FoodItem(int price, String name) {
        super(price, name);
    }

    public FoodItem(int price, String name, boolean isHealthy) {
        super(price, name);
        this.isHealthy = isHealthy;
    }

    public void setHealthy(boolean healthy) {
        isHealthy = healthy;
    }

    @Override
    public String toString() {
        return "FoodItem{" +
                "price=" + price +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        FoodItem foodItem = (FoodItem) o;
        return isHealthy == foodItem.isHealthy;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), isHealthy);
    }
}
