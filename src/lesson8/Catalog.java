package lesson8;

import java.util.Arrays;
import java.util.Objects;

/**
 * @author Rustam Khakov
 */
public class Catalog {
    Item[] items = new Item[3];
    private String name;
///название каталога
    public Catalog(Item[] items) {
        this.items = items;
    }

    public Item[] getItems() {
        return items;
    }

    public void setItems(Item[] items) {
        this.items = items;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Catalog catalog = (Catalog) o;
        return Arrays.equals(items, catalog.items) &&
                Objects.equals(name, catalog.name);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name);
        result = 31 * result + Arrays.hashCode(items);
        return result;
    }
}
