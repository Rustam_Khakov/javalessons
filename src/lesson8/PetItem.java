package lesson8;

import java.util.Objects;

/**
 * @author Rustam Khakov
 */
public class PetItem extends Item {
    private boolean isEatable;

    public PetItem(int price, String name) {
        super(price, name);
    }

    public PetItem(int price, String name, boolean isEatable) {
        super(price, name);
        this.isEatable = isEatable;
    }

    public void setEatable(boolean eatable) {
        isEatable = eatable;
    }

    @Override
    public String toString() {
        return "PetItem{" +
                "price=" + price +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PetItem petItem = (PetItem) o;
        return isEatable == petItem.isEatable;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), isEatable);
    }
}
