package lesson8;

import HW7.Cat;

import java.util.Scanner;

/**
 * @author Rustam Khakov
 */
public class Test {
    public static void main(String[] args) {
        Item[] foodItems = new Item[3];
        foodItems[0] = new FoodItem(1, "хлеб", false);
        foodItems[1] = new FoodItem(3, "масло", true);
        foodItems[2] = new FoodItem(4, "сыр", true);

        Item[] nonFoodItems = new Item[3];
        nonFoodItems[0] = new NonFoodItem(3, "дрель", 10);
        nonFoodItems[1] = new NonFoodItem(5, "ключ", 3);
        nonFoodItems[2] = new NonFoodItem(6, "бумага",1);

        Item[] petItems = new Item[3];
        petItems[0] = new PetItem(8, "хомяк");
        petItems[1] = new PetItem(9, "попугай");
        petItems[2] = new PetItem(12, "кролик");

        Catalog foodItemCatalog = new Catalog(foodItems);
        foodItemCatalog.setName("Каталог еды");
        Catalog nonFoodItemCatalog = new Catalog(nonFoodItems);
        nonFoodItemCatalog.setName("Каталог инструмантов");
        Catalog petItemsCatalog = new Catalog(petItems);
        petItemsCatalog.setName("каталог животных");
        // напишите здесь каталог для животных

/// a==a -> true
        // a == b == c
        // a == null -> false
        // .equals() -один и тот же ответ
        // a== b b==a


        Shop shop = new Shop(foodItemCatalog, petItemsCatalog, nonFoodItemCatalog);
        shop.setAddress("Кремлевская 10");
        shop.setName("5ka");
        Shop shop1 = new Shop(null, null, null);
        shop1.setAddress("Кремлевская 10");
        shop1.setName("5ka");
        System.out.println(shop1.equals(shop) + " равны");
        System.out.println(foodItemCatalog.equals(foodItemCatalog) + " равны");
        System.out.println(foodItemCatalog.equals(nonFoodItemCatalog));
        System.out.println(petItems.equals(petItems) + " равны");
        System.out.println(petItems.equals(nonFoodItems));
        PetItem parrot = new PetItem(9, "попугай",false);
        PetItem parrot2 = new PetItem(9, "попугай",false);
        PetItem parrot3 = new PetItem(10, "попугай",false);


        System.out.println(parrot.equals(parrot2) + " равны");
        System.out.println(parrot2.equals(parrot3));

//        Scanner scanner = new Scanner(System.in);
//        String catalogName = scanner.next();
//        Catalog catalogByName = shop.getCatalogByName(catalogName);
//       // shop.getFoodCatalog().equals(shop.getNonFoodCatalog());
//
//        Item[] foods = catalogByName.getItems();
//        for (int i = 0; i < foods.length; i++) {
//            System.out.println(foods[i]);
//        }

    }
}
