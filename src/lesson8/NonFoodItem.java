package lesson8;

import java.util.Objects;

/**
 * @author Rustam Khakov
 */
public class NonFoodItem extends Item {
    private int size;

    public NonFoodItem(int price, String name) {
        super(price, name);
    }

    public NonFoodItem(int price, String name, int size) {
        super(price, name);
        this.size = size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "NonFoodItem{" +
                "price=" + price +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        NonFoodItem that = (NonFoodItem) o;
        return size == that.size;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), size);
    }
}
