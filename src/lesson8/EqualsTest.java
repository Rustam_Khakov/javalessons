package lesson8;

/**
 * @author Rustam Khakov
 */
public class EqualsTest {
    public static void main(String[] args) {
        Table tableOne = new Table(2,2,2);
        Table tableTwo = new Table(2,2,2);
        Table tableThree = tableOne;
        if (tableOne.equals(tableTwo)) {
            System.out.println("столы равные");
        } else {
            System.out.println("столы разные");
        }

        // tableOne.equals(tableTwo)    tableTwo.equals(tableOne)
        // a == b == c     a == b    b == c     c == a

        // a == a
        System.out.println(tableOne.equals(tableOne));
        /// a == b                          b == a
        System.out.println(tableOne.equals(tableTwo));
        System.out.println(tableTwo.equals(tableOne));
        // a == b == c     a == b    b == c     c == a
        System.out.println(tableOne.equals(tableTwo));
        System.out.println(tableTwo.equals(tableThree));
        System.out.println(tableThree.equals(tableOne));
        //a == null  -> false
        System.out.println(tableOne.equals(null));
    }
}
