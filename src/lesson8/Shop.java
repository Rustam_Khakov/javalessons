package lesson8;

import HW7.Cat;

import java.util.Objects;

/**
 * @author Rustam Khakov
 */
public class Shop {
    Catalog foodCatalog;
    Catalog petCatalog;
    Catalog nonFoodCatalog;
    private String name;
    private String address;

    //добавьте название магазина

    public Shop(Catalog foodCatalog, Catalog petCatalog, Catalog nonFoodCatalog) {
        this.foodCatalog = foodCatalog;
        this.petCatalog = petCatalog;
        this.nonFoodCatalog = nonFoodCatalog;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Catalog getFoodCatalog() {
        return foodCatalog;
    }

    public void setFoodCatalog(Catalog foodCatalog) {
        this.foodCatalog = foodCatalog;
    }

    public Catalog getPetCatalog() {
        return petCatalog;
    }

    public void setPetCatalog(Catalog petCatalog) {
        this.petCatalog = petCatalog;
    }

    public Catalog getNonFoodCatalog() {
        return nonFoodCatalog;
    }

    public void setNonFoodCatalog(Catalog nonFoodCatalog) {
        this.nonFoodCatalog = nonFoodCatalog;
    }

    public Catalog getCatalogByName(String catalogName){
        switch (catalogName) {
            case "Еда" :return foodCatalog;
            case "Не еда": return nonFoodCatalog;
            case "Животные": return petCatalog;
            default:return null;

        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shop shop = (Shop) o;
        return Objects.equals(name, shop.name) &&
                Objects.equals(address, shop.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, address);
    }
}
