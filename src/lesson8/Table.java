package lesson8;

import java.util.Objects;

/**
 * @author Rustam Khakov
 */
public class Table {
    private int high;
    private int width;
    private int length;

    public Table(int high, int width, int length) {
        this.high = high;
        this.width = width;
        this.length = length;
    }

//    @Override
//    public boolean equals(Object obj) {
//        if (obj == this) {
//            return true;
//        }
//        if (obj == null) {
//            return false;
//        }
//        if (getClass() != obj.getClass()) {
//            return false;
//        }
//        Table table = (Table) obj;
//        return this.high == table.high && table.length == this.length && table.width == this.width;
//    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Table table = (Table) o;
        return high == table.high &&
                width == table.width &&
                length == table.length;
    }

    @Override
    public int hashCode() {
        return Objects.hash(high, width, length);
    }
}
