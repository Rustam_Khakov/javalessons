package lesson8;

/**
 * @author Rustam Khakov
 */
public class Child extends Parent {
    static String childMeta = "метадата ребенка"; //третье
    static {
        System.out.println("Статический блок ребенка");//четвертое
    }

    String name = "что-то"; // седьмое
    public Child() { // восьмое
        System.out.println("конструктор ребенка");
    }
}
