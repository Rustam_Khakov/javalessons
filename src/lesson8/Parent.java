package lesson8;

/**
 * @author Rustam Khakov
 */
public class Parent {
    static String metadata = "параметр я родитель";// первое

    static {
        System.out.println("статический блок");//второе
    }

    int age = 5; //пятое
    public Parent() { // шестое
        System.out.println("я конструктор родителя");
    }
}
