package lesson11;

import java.util.Random;

/**
 * @author Rustam Khakov
 */
public class StackOverflowErrorTest implements Cloneable{
    TestString test;

    public StackOverflowErrorTest(TestString test) {
        this.test = test;
    }

    public TestString getTest() {
        return test;
    }

    public void setTest(TestString test) {
        this.test = test;
    }

    public static void main(String[] args) {
        TestString one = new TestString("один");
        StackOverflowErrorTest test = new StackOverflowErrorTest(one);
        test.update((TestString) one.clone());
        System.out.println(one.getStr());
    }

    private void update(TestString one) {
        one.setStr("два");
    }

    @Override
    protected Object clone() {
        return new StackOverflowErrorTest(this.test);
    }
}
