package lesson11;

/**
 * @author Rustam Khakov
 */
public class TestString implements Cloneable{

    private String str;

    public TestString(String str) {
        this.str = str;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    @Override
    protected Object clone() {
        return new TestString(this.str);
    }
}
