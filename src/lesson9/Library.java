package lesson9;

/**
 * @author Rustam Khakov
 */
public class Library {
    public static void main(String[] args) {
        Publish[] publishes = new Publish[4];
        publishes[0] = new Book(450, "Толстой", 1887, "Война и мир 1 том", "Мир");
        publishes[1] = new Journal(39, "Наука и техника", 1997,"Наука и техника", "Печать");
        publishes[2] = new Book(470, "Толстой", 1887, "Война и мир 2 том", "Мир");
        publishes[3] = new Journal(36, "Наука и техника", 1997,"Наука и техника лучшее", "Печать");
        Publish maxPageCountPublish = publishes[0];
        //for -> if ->maxPageCountPublish.getPageCount() < publishes[i].getPageCount()
        // -> maxPageCountPublish = publishes[i]
        for (Publish publish : publishes) {
            if (maxPageCountPublish.getPageCount() < publish.getPageCount()) {
                maxPageCountPublish = publish;
            }
            if (publish instanceof Book) {
                System.out.println(publish);
            }
        }
        System.out.println(maxPageCountPublish);
        System.out.println("максимальная толщина: " + maxPageCountPublish.getPageCount());

    }
}
