package lesson9;

/**
 * @author Rustam Khakov
 */
public class Calculator {
    public double add(int first, int second) {
        return first + second;
    }

    public double add(double first, double second) {
        return first+ second;
    }

    public double add(int second, double first) {
        return first + second;
    }

    public double add(double second, int first) {
        return first + second;
    }

}
