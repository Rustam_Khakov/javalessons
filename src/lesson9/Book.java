package lesson9;

/**
 * @author Rustam Khakov
 */
public class Book implements Publish {
    //название автор год издания колво страниц и издательство
    int pageCount;
    public String author;
    public int yearOfPublication;
    public String title;
    public String publishingHouse;

    public Book(int pageCount, String author, int yearOfPublication, String title, String publishingHouse) {
        this.pageCount = pageCount;
        this.author = author;
        this.yearOfPublication = yearOfPublication;
        this.title = title;
        this.publishingHouse = publishingHouse;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getYearOfPublication() {
        return yearOfPublication;
    }

    public void setYearOfPublication(int yearOfPublication) {
        this.yearOfPublication = yearOfPublication;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }

    public void setPublishingHouse(String publishingHouse) {
        this.publishingHouse = publishingHouse;
    }

    @Override
    public String toString() {
        return "Book{" +
                "pageCount=" + pageCount +
                ", author='" + author + '\'' +
                ", yearOfPublication=" + yearOfPublication +
                ", title='" + title + '\'' +
                ", publishingHouse='" + publishingHouse + '\'' +
                '}';
    }

    @Override
    public int getPageCount() {
        return pageCount;


    }

}
