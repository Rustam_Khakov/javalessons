package lesson9;

/**
 * @author Rustam Khakov
 */
public interface Table {

    int getRows();

    int getColumns();

    int getItem(int row, int column);

    void addItem(int row, int column, int value);

    Table sum(Table other);

    Table minus(Table other);

    void fillWithRandom(int from, int to);

    void print();

    int getMaxValue();

    Table clone();

}
