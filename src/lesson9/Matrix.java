package lesson9;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

/**
 * @author Rustam Khakov
 */
public class Matrix implements Table {
    int[][] array;
    int rows;
    int columns;

    public Matrix(int row, int column) {
        this.rows = row;
        this.columns = column;
        this.array = new int[row][column];
    }

    @Override
    public int getRows() {
        return rows;
    }

    @Override
    public int getColumns() {
        return columns;
    }

    @Override
    public int getItem(int row, int column) {
        if (row> this.rows || column > this.columns) {
            System.out.println(String.format("матрица меньшего размера %s %s", rows, columns));
            return -1;
        }
        return array[row][column];
    }

    @Override
    public void addItem(int row, int column, int value) {
        if (row> this.rows || column > this.columns) {
            System.out.println(String.format("матрица меньшего размера %s %s", rows, columns));
            return;
        }
        this.array[row][column] = value;
    }

    @Override
    public Table sum(Table other) {
        if (other.getRows() != this.rows || getColumns() != this.columns) {
            System.out.println(String.format("матрица меньшего размера %s %s", rows, columns));
            return null;
        }
        //array -> for -> for -> array[i][j] += other.getItem(i, j);
        for (int i = 0; i < this.array.length; i++) {
            for (int j = 0; j < this.array[i].length; j++) {
                this.array[i][j] += other.getItem(i, j);
            }
        }
        return this;
    }

    @Override
    public Table minus(Table other) {
        if (other.getRows() != this.rows || getColumns() != this.columns) {
            System.out.println(String.format("матрица меньшего размера %s %s", rows, columns));
            return null;
        }
        for (int i = 0; i < this.array.length; i++) {
            for (int j = 0; j < this.array[i].length; j++) {
                this.array[i][j] -= other.getItem(i, j);
            }
        }
        return this;
    }

    @Override
    public void fillWithRandom(int from, int to) {
        Random random = new Random();
        //array -> for -> for -> array[i][j] =
        //random.nextInt(to - from) + from;
        for (int i = 0; i < this.array.length; i++) {
            for (int j = 0; j < this.array[i].length; j++) {
                this.array[i][j] += random.nextInt(to - from) + from;
            }
        }
    }

    @Override
    public void print() {
        //array -> for -> for -> sout()
        for (int i = 0; i < this.array.length; i++) {
            for (int j = 0; j < this.array[i].length; j++) {
                System.out.print(this.array[i][j] + " ");
            }
            System.out.println();
        }

    }

    @Override
    public int getMaxValue() {
        int max = array[0][0];
        for (int i = 0; i < this.array.length; i++) {
            for (int j = 0; j < this.array[i].length; j++) {
                if (array[i][j] > max) {
                    max = array[i][j];
                }
            }
        }
        return max;
    }

    @Override
    public Table clone() {
        Table table = new Matrix(this.rows, this.columns);
        //array -> for -> for -> table.addItem(i, j, array[i][j])
        for (int i = 0; i < this.array.length; i++) {
            for (int j = 0; j < this.array[i].length; j++) {
                table.addItem(i, j, this.array[i][j]);
            }
        }
        return table;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Matrix matrix = (Matrix) o;
        return rows == matrix.rows &&
                columns == matrix.columns &&
                Arrays.equals(array, matrix.array);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(rows, columns);
        result = 31 * result + Arrays.hashCode(array);
        return result;
    }
}
