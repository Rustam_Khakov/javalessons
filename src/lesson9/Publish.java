package lesson9;

/**
 * @author Rustam Khakov
 */
public interface Publish {
    int getPageCount();
}
