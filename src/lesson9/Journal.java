package lesson9;

/**
 * @author Rustam Khakov
 */
public class Journal implements Publish {
    // создать переменные и метод toString(){}
    // название атвор год издания колво страниц и издательство колво страниц
    int pageCount;
    public String author;
    public int yearOfPublication;
    public String title;
    public String publishingHouse;

    public Journal(int pageCount, String author, int yearOfPublication, String title, String publishingHouse) {
        this.pageCount = pageCount;
        this.author = author;
        this.yearOfPublication = yearOfPublication;
        this.title = title;
        this.publishingHouse = publishingHouse;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getYearOfPublication() {
        return yearOfPublication;
    }

    public void setYearOfPublication(int yearOfPublication) {
        this.yearOfPublication = yearOfPublication;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }

    public void setPublishingHouse(String publishingHouse) {
        this.publishingHouse = publishingHouse;
    }

    @Override
    public int getPageCount() {
        return pageCount;
    }

    @Override
    public String toString() {
        return "Journal{" +
                "pageCount=" + pageCount +
                ", author='" + author + '\'' +
                ", yearOfPublication=" + yearOfPublication +
                ", title='" + title + '\'' +
                ", publishingHouse='" + publishingHouse + '\'' +
                '}';
    }
}
