package lesson16;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author Rustam Khakov
 */
public class Server {

    //192.168.0.106
    public static void main(String[] args) throws IOException {
        try (ServerSocket server = new ServerSocket(1234)) {
            System.out.println(server.getInetAddress());
            System.out.println(server.getLocalSocketAddress());
            System.out.println(server.getLocalPort());
            while (true) {
                try (Socket socket = server.accept();
                     BufferedReader is =
                             new BufferedReader(
                                     new InputStreamReader(
                                             socket.getInputStream()
                                     )
                             );
                ) {
                    if (is.ready()) {
                        System.out.println(is.readLine());
                    }
                }
            }
        }
    }
}
