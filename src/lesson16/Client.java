package lesson16;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 * @author Rustam Khakov
 */
public class Client {
    private static Socket client;


    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        //String host = scanner.next();
        //Integer port = scanner.nextInt();
        client = new Socket("127.0.0.1", 1234);
        try (PrintWriter os = new PrintWriter(
                client.getOutputStream(), true)
        ) {
            System.out.println(client.isBound());
            System.out.println(client.isConnected());
            os.println("Я в зале");
            client.close();
        }
    }
}
