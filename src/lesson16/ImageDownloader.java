package lesson16;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * @author Rustam Khakov
 */
public class ImageDownloader {

    public static void main(String[] args) throws IOException {
        String name = "1.html";
        String path = "https://stackoverflow.com/questions/5882005/how-to-download-image-from-any-web-page-in-java";
        URL url = new URL(path);
        try (InputStream is = new BufferedInputStream(url.openStream());
             FileOutputStream os = new FileOutputStream(name);) {
            int read;
            while ((read = is.read()) != -1) {
                os.write(read);
            }
        }
    }
}
