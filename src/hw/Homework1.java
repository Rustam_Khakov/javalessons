package hw;

public class Homework1 {
    public static void main(String[] args) {
        int radius = 5;
        double pi = 3.14;

        Math.pow(radius, 2); // radius * radius
        double space = pi * Math.pow(radius, 2);
        double perimeter = 2 * pi * radius;

        String str1 = String.format("Площадь круга с радиусом 5 равна " + Math.floor(space));
        String str2 = String.format("Периметр круга с радиусом 5 равна " + Math.floor(perimeter));
        System.out.println("Площадь круга с радиусом 5 равна " + Math.floor(space));
        System.out.println(str2);

    }
}
