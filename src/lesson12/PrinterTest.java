package lesson12;

/**
 * @author Rustam Khakov
 */
public class PrinterTest {
    public static void main(String[] args) {
        Printer<Integer> integerPrinter = new Printer<>();
        int[] numbers = new int[3];
        integerPrinter.print(12);
        integerPrinter.printInt(12);
        integerPrinter.printInt(12, 13);
        integerPrinter.printInt(numbers);


    }
}
