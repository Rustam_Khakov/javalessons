package lesson12;

/**
 * @author Rustam Khakov
 */
public class Printer<T extends Number> {

    public void print(T print) {
        System.out.println(print);
    }

    public void printInt(int... print) {
        int[] vals = print;
        if (vals!= null) {
            for (int val: vals) {
                System.out.println(val);
            }
        }
    }


}
