package lesson12;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Rustam Khakov
 */
public class MyArrayListTest {
    public static void main(String[] args) {
        List<String> first = new ArrayList<>();
        first.add("Wow");
        first.add("Hi");
        List<String> list = new MyArrayList<>(new String[4]);
        list.add("Test");
        list.add("Test2");
        list.add("Test3");
        list.add("Test4");
        list.add("Test5");
        System.out.println(list.size() == 5);
        System.out.println(list.get(4));
        System.out.println(list.set(0, "Test1"));
        list.addAll(first);
        System.out.println(list.size() == 7);
        list.remove("Hi");
        System.out.println(list.size() == 6);

    }
}
