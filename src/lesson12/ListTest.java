package lesson12;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Rustam Khakov
 */
public class ListTest {
    public static void main(String[] args) {
        String[] array = new String[16];
        ///
        array = increaseSize(array);
        List<String> list = new ArrayList<>();
        list.size();// array.length
        // list.get(0);// array[0]
        for (int i = 0; i < array.length; i++) {
            String s = array[i];
        }

        for (int i = 0; i < list.size(); i++) {
            list.get(i);
        }

        for (String str : array) {

        }

        for (String str : list) {

        }


        list.add("First");
        list.add("First");
        list.add("First");
        list.add("First");
        list.add("First");
        list.add("First");
        //System.out.println(list.size());

        List<String> strings = addTest(new ArrayList<>());
        List<String> strings1 = addTest(new LinkedList<>());
        addTestGet(strings);
        addTestGet(strings1);


    }

    private static List<String> addTest(List<String> list) {
        long start = System.currentTimeMillis();
        for (int i = 0; i< 100_000; i++) {
            list.add("test");
        }
        long stop = System.currentTimeMillis() - start;
        System.out.println(stop);
        return list;
    }


    private static void addTestGet(List<String> list) {
        long start = System.currentTimeMillis();
        for (int i = 0; i< 100_000; i++) {
            list.get(i);
        }
        long stop = System.currentTimeMillis() - start;
        System.out.println(stop);
    }






    private static String[] increaseSize(String[] array) {
        String[] arrayNew = new String[array.length * 2];
        for (int i = 0; i < array.length; i++) {
            arrayNew[i] = array[i];
        }
        return arrayNew;
    }
}
