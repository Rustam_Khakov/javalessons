package lesson12;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * @author Rustam Khakov
 */
public class MyArrayList<T> implements List<T> {
    T[] array;
    int size = 0;
    int maxSize;
    int changeSize;

    public MyArrayList(T[] array) {
        this.array = array;
        this.maxSize = array.length;
        this.changeSize = maxSize *4/3;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }


    //реализовать
    // добавлять в массив значение t, если size > changeSize
    //метод который увеличивает массив в 2 раза

    @Override
    public boolean add(T t) {
        return false;
    }
    @Override
    //надо
    public T get(int index) {
        return null;
    }

    @Override
    //добавть по индексу параметр
    public T set(int index, T element) {
        return null;
    }

    @Override
    public void add(int index, T element) {

    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    // удалить элемент
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public T remove(int index) {
        return null;
    }

    /*
    Все что ниже не надо, если есть желание можете попробовать реализовать
     */
    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return null;
    }

    /*
    Все что ниже не надо
     */

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }
}
