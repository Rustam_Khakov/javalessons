package lesson12;

/**
 * @author Rustam Khakov
 */
public class StringTest {
    public static void main(String[] args) {
        String first = "first"; /// посмотрите в памяти есть ли там строка "first" если нет то создай
        String second = "first";// если есть то укажи на нее
        String third = new String("first").intern();
        System.out.println(first.equals(third));

    }
}
