package lesson14;

/**
 * @author Rustam Khakov
 */
public class Dog extends Animal implements Eatable, Pet {
    @Override
    protected void doEat() {
        System.out.println("я кушаю");
    }

    @Override
    protected void catchEat() {
        System.out.println("я поймал еду");
    }

    @Override
    protected boolean isHungry() {
        return super.isHungry();
    }
}
