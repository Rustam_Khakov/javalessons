package lesson14;

/**
 * @author Rustam Khakov
 */
public abstract class Animal implements Eatable {
    private int size;
    public Animal() {

    }

    @Override
    public void eat() {
        if (isHungry()) {
            catchEat();
            doEat();
        }
    }

    protected abstract void doEat();

    protected abstract void catchEat();

    protected boolean isHungry() {
        return false;
    }
}
