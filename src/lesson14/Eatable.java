package lesson14;

/**
 * @author Rustam Khakov
 */
public interface Eatable {
    void eat();
}
