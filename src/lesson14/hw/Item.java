package lesson14.hw;

import lesson14.Category;

/**
 * @author Rustam Khakov
 */
public class Item implements Comparable<Item> {
    int price;
    double rate;
    String name;
    Category category;

    public Item(int price, double rate, String name, Category category) {
        this.price = price;
        this.rate = rate;
        this.name = name;
        this.category = category;
    }

    @Override
    public int compareTo(Item o) {
        // 0 - объекты равны
        // больше 0 наш объект в сортировке идет после 1
        // меньше 0 то в сортировке наш элемент раньше -1
        return this.price - o.price;
    }

    @Override
    public String toString() {
        return "Item{" +
                "price=" + price +
                ", rate=" + rate +
                ", name='" + name + '\'' +
                ", category=" + category +
                '}';
    }
}
