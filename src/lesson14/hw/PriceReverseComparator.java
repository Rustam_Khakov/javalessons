package lesson14.hw;

import lesson14.hw.Item;

import java.util.Comparator;

/**
 * @author Rustam Khakov
 */
public class PriceReverseComparator implements Comparator<Item> {
    @Override
    public int compare(Item o1, Item o2) {
//        if (o1.price > o2.price) {
//            return -1;
//        }
//        if (o1.price == o2.price) {
//            return 0;
//        }
//        if (o1.price < o2.price) {
//            return 1;
//        }
        return o2.price - o1.price;
    }
}
