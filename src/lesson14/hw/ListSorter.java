package lesson14.hw;

import java.util.Collections;
import java.util.List;

/**
 * @author Rustam Khakov
 */
public class ListSorter {

    public static List<Item> sortByPrice(List<Item> items){
        Collections.sort(items);
        return items;
    }

    public static List<Item> sortByPriceReversed(List<Item> items){
        Collections.sort(items, new PriceReverseComparator());
        return items;
    }

    public static List<Item> sortByCategory(List<Item> items){
        Collections.sort(items, new CategorySortComparator());
        return items;
    }

    public static List<Item> sortByCategoryReversed(List<Item> items){
        Collections.sort(items, new CategorySortComparator().reversed());
        return items;
    }
}
