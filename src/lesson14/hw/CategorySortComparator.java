package lesson14.hw;

import lesson14.hw.Item;

import java.util.Comparator;

/**
 * @author Rustam Khakov
 */
public class CategorySortComparator implements Comparator<Item> {
    @Override
    public int compare(Item o1, Item o2) {
        return o1.category.ordinal() - o2.category.ordinal();
    }
}
