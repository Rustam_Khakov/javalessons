package lesson14.hw;

import lesson14.Category;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Rustam Khakov
 */
public class ComparatorSortTest {
    public static void main(String[] args) {
        Set<Item> set = new TreeSet<>();
        set.add(new Item(12, 3.4,"Dell", Category.COMPUTER));
        set.add(new Item(10, 3.7,"Lenovo", Category.COMPUTER));
        set.add(new Item(16, 4.0,"Mac", Category.COMPUTER));
        set.add(new Item(17, 4.8,"Sony", Category.VIDEO));
        set.add(new Item(21, 5.0,"Apple", Category.AUDIO));
        set.add(new Item(8, 2.1,"Beats", Category.AUDIO));
        List<Item> list = new ArrayList<>(set);
        for (Item item : list) {
            System.out.println(item);
        }
        System.out.println("___________");
        list = ListSorter.sortByCategory(list);
        for (Item item : list) {
            System.out.println(item);
        }
        System.out.println("___________");
        list = ListSorter.sortByCategoryReversed(list);
        for (Item item : list) {
            System.out.println(item);
        }
    }
}
