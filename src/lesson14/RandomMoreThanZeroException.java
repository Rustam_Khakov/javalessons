package lesson14;

/**
 * @author Rustam Khakov
 */
public class RandomMoreThanZeroException extends RuntimeException {
    public RandomMoreThanZeroException(String message) {
        super(message);
    }
}
