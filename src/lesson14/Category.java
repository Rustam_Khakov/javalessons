package lesson14;

/**
 * @author Rustam Khakov
 */
public enum Category {
    AUDIO, VIDEO, COMPUTER
}
