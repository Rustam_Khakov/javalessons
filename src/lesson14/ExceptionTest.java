package lesson14;

import java.util.Random;

/**
 * @author Rustam Khakov
 */
public class ExceptionTest {

    public static void main(String[] args) {
        try {
            testAction();
        } catch (RuntimeException e) {
            System.out.println("Test catch");
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("тест 2");
        } finally {
            System.out.println("Я выполнился");
        }

    }

    public static void testAction() throws Exception {
        Random random = new Random();
        int i = random.nextInt(2);
        if (i >0) {
            throw new RandomMoreThanZeroException("сгенерировали число %s".formatted(i));
        } else {
            throw new Exception("число 0");
        }

    }
}
