package lesson4;

/**
 * @author Rustam Khakov
 */
public class HW4 {
    public static void main(String[] args) {
        int[][] arr = new int[8][8];
        //заполнили массив
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = i - j* j;
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
//Выполняем что-то
        int max= Integer.MIN_VALUE;
        int maxLineIndex = 0;
        for (int i = 0; i < arr.length; i++) { // по строкам
            int sum = 0;
            for (int j = 0; j < arr[i].length; j++) { // по столбцам
                sum += arr[i][j];
            }
            if (sum > max) {
                max = sum;
                maxLineIndex = i;
            }
        }
        System.out.println(maxLineIndex);
    }


}
