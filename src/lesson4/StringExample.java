package lesson4;

/**
 * @author Rustam Khakov
 */
public class StringExample {
    public static void main(String[] args) {
        String first = "fsf";
        String sec = "fds";
        String res = first + sec;
        char[] symbols = {'a', 'b', 'c', 'd'};
        String charStr = new String(symbols);
        System.out.println(charStr);
        System.out.println(res);
        charStr.length();
        boolean isStartWithAb = charStr.startsWith("ab");
        System.out.println(isStartWithAb);
        System.out.println(charStr.endsWith("b"));
        System.out.println(charStr.toUpperCase());
        System.out.println(charStr.contains("@"));
        charStr.charAt(0);

    }
}
