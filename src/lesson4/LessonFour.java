package lesson4;

/**
 * @author Rustam Khakov
 */
public class LessonFour {

    public static void main(String[] args) {
        System.out.println(args.length);
        for (int i = 0; i < args.length; i++) {
            System.out.println(args[i]);
        }
        int[][] twoDimArray = new int[2][2];
        int length = twoDimArray.length;// сколько строк
        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[i].length; j++) {
                twoDimArray[i][j] = i + j;
                System.out.print(twoDimArray[i][j] + " ");
            }
            System.out.println();
        }

        int[][] secArray = new int[2][2];
        for (int i = 0; i < secArray.length; i++) {
            for (int j = 0; j < secArray[i].length; j++) {
                secArray[i][j] = i * j;
            }
        }

        int[][] thirdArr = new int[2][2];
        for (int i = 0; i < thirdArr.length; i++) {
            for (int j = 0; j < thirdArr[i].length; j++) {
                thirdArr[i][j] = twoDimArray[i][j] - secArray[i][j];
                System.out.print(thirdArr[i][j] + " ");
            }
            System.out.println();
        }

        int[][] fouthArr = {
                {1, 2},
                {2, 3, 4},
                {5, 4, 5, 6, 5}
        };
        for (int i = 0; i < fouthArr.length; i++) {
            for (int j = 0; j < fouthArr[i].length; j++) {
                System.out.print(fouthArr[i][j] + " ");
            }
            System.out.println();
        }

//
        int[][] fithArr = new int[8][8];
        for (int i = 0; i < fithArr.length; i++) {
            for (int j = 0; j < fithArr[i].length; j++) {
                fithArr[i][j] = i *3 + j*2;
            }
            System.out.println();
        }

        for (int i = 0; i < fithArr.length; i++) {

            for (int j = 0; j < fithArr[i].length; j++) {
                if(i == fithArr.length - 1 - j) {

                }
                System.out.print(fithArr[i][j] + " ");
            }
        }

    }
}
