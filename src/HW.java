/**
 * @author Rustam Khakov
 */
public class HW {
    public static void main(String[] args) {
        //Task 1
        double deposit = 200.0;
        double percent = 5;
        int months = 12;
        int years = 10;
        double sumWithoutCap = deposit;
        double sumWithMonthCap = deposit;
        double sumWithYearCap = deposit;
        for (int year = 0; year < years; year++) {
            sumWithoutCap = sumWithoutCap + deposit * (percent / 100); // 5% - 5/100 = percent/100
            sumWithYearCap = sumWithYearCap + sumWithYearCap * (percent / 100);
            for (int month = 0; month < months; month++) {
                sumWithMonthCap = sumWithMonthCap + sumWithMonthCap * (percent / months / 100);
            }
        }
        System.out.println(sumWithMonthCap);
        System.out.println(sumWithYearCap);
        System.out.println(sumWithoutCap);

    }
}
