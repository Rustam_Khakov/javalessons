package lesson17;

/**
 * @author Rustam Khakov
 */
public class Lesson17 {
    public static void main(String[] args) {
        String val = "wertyu";
        if (val instanceof String) {
            System.out.println("String");
        } else {
            System.out.println("Not a string");
        }
    }
}
