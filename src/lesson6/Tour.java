package lesson6;

/**
 * @author Rustam Khakov
 */
public class Tour {
    private int star;
    String country;
    String city;
    String hotelName;
    int price;

    public Tour(String[] tourParams) {
        this.country = tourParams[0];
        this.star = Integer.parseInt(tourParams[1]);
        this.hotelName = tourParams[2];
        this.city = tourParams[3];
        this.price = Integer.parseInt(tourParams[4]);
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }

    @Override
    public String toString() {
        return "Tour{" +
                "star=" + star +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", hotelName='" + hotelName + '\'' +
                ", price=" + price +
                '}';
    }
}
