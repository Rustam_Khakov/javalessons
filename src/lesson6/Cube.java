package lesson6;

/**
 * @author Rustam Khakov
 */
public class Cube extends Shape{
    private int side;

    public Cube() {

    }

    public Cube(int side) {
        this.side = side;
    }

    public int getSide() {
        return side;
    }

    public void setSide(int side) {
        this.side = side;
    }
    @Override
    public double calcPerimeter() {
        return 12 * side;
    }
    @Override
    public double calcSquare() {
        return 6*(side * side);
    }

    @Override
    public String toString() {
        return "Cube{" +
                "side=" + side +
                '}';
    }
}
