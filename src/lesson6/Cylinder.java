package lesson6;

/**
 * @author Rustam Khakov
 */
public class Cylinder extends Shape {
    private int height, rad;

    public Cylinder() {

    }

    public Cylinder(int heightC, int rad) {
        this.height = heightC;
        this.rad = rad;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int heightC) {
        this.height = heightC;
    }

    public int getRad() {
        return rad;
    }

    public void setRad(int rad) {
        this.rad = rad;
    }


    @Override
    public double calcSquare() {
        return 2 * Math.PI * rad * height + 2 * Math.PI * (rad * rad);
    }

    @Override
    public String toString() {
        return "Cylinder{" +
                "height=" + height +
                ", rad=" + rad +
                '}';
    }
}
