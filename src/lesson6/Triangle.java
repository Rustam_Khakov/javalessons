package lesson6;

/**
 * @author Rustam Khakov
 */
public class Triangle extends Shape {
    //три стороны площадь треуг = sqrt(p(p-a)(p-b)(p-c)) - формула Герона
    //p = perimeter/2   Math.sqrt() - корень
    //double
    private double firstSide;
    private double secondSide;
    private double thirdSide;

    public Triangle(int firstSide, int secondSide, int thirdSide) {
        this.firstSide = firstSide;
        this.secondSide = secondSide;
        this.thirdSide = thirdSide;
    }

    @Override
    public double calcPerimeter() {
        return thirdSide + secondSide + firstSide;
    }

    @Override
    public double calcSquare() {
        double semiPerimeter = calcPerimeter() / 2;
        return Math.sqrt(semiPerimeter * (semiPerimeter - firstSide) *
                (semiPerimeter - secondSide) * (semiPerimeter - thirdSide));
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "firstSide=" + firstSide +
                ", secondSide=" + secondSide +
                ", thirdSide=" + thirdSide +
                '}';
    }
}
