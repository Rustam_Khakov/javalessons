package lesson6;

import java.awt.*;
import java.util.Scanner;

/**
 * @author Rustam Khakov
 */
public class FigureGenerator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String figure = scanner.next();
        Shape shape = null;
        switch (figure) {
            case "Треугольник":
                System.out.println("стороны треугольника:");
                int side1 = scanner.nextInt();
                int side2 = scanner.nextInt();
                int side3 = scanner.nextInt();
                shape = new Triangle(side1, side2, side3);
                break;
            case "Круг":
                System.out.println("Введите радиус круга:");
                int radius = scanner.nextInt();
                shape = new Circle(radius);
                break;
            case "Квадрат":
                System.out.println("Введите сторону квадрата:");
                int side = scanner.nextInt();
                shape = new Square(side);
                break;
            case "Прямоугольник":
                System.out.println("Введите первую сторону прямоугольника:");
                int sideRec = scanner.nextInt();
                System.out.println("Введите вторую сторону прямоугольника:");
                int sideRec2 = scanner.nextInt();
                shape = new Rectangle(sideRec, sideRec2);
                break;
            case "Куб":
                System.out.println("Введите сторону куба:");
                int sideCube = scanner.nextInt();
                shape =  new Cube(sideCube);
                break;
            case "Цилиндр":
                System.out.println("Введите высоту цилиндра:");
                int height = scanner.nextInt();
                System.out.println("Введите радиус основания круга:");
                int rad = scanner.nextInt();
                shape = new Cylinder(height,rad);
            default:
                shape = null;
        }
        if (shape != null) {
            System.out.println(shape);
            System.out.println("Периметр фигуры " + shape.calcPerimeter());
            System.out.println("Площадь фигуры " + shape.calcSquare());
        }

    }
}
