package lesson6;

import java.util.Scanner;

/**
 * @author Rustam Khakov
 */
public class ToursChecker {
    public static void main(String[] args) {
        String[][] tours = {
                {"Турция", "5", "Spa and Resort", "Side", "32000"},
                {"Турция", "3", "Spa and Resort 3", "Side", "20000"},
                {"Египет", "4", "Spa and Resort 4", "Sharm", "30000"},
                {"Италия", "3", "Hotel inn", "Rome", "30000"}
        };
        Scanner scanner = new Scanner(System.in);
        int star = scanner.nextInt();
        int priceMax = scanner.nextInt();
        for (int i = 0; i < tours.length; i++) {
            Tour tour = new Tour(tours[i]);
            if (tour.getStar() >= star && tour.price <= priceMax) {
                System.out.println(tour);
            } else if (tour.price <= priceMax * 110 / 100) {
                System.out.println("Чуть дороже " + tour);
            }
        }
    }
}
