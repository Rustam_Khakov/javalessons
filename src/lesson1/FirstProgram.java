package lesson1;

/**
 * @author Rustam Khakov
 */
public class FirstProgram {
    public static void main(String[] args) {
        int firstParam = 3 + 4;
        int secondParam = 5;
        int sum = firstParam + secondParam;
        byte byteValue = 127;
        byteValue++;//byteValue += 1  //byteValue = byteValue + 1
        byteValue += 5;

        char symbol1 = 97;//
        char symbol2 = 500;
        byte fromInt = (byte)700;
        double numberWithDot = 2.345678906789;
        double secondNumberWithDot = 3.3;
        int minusResult = (int)(secondNumberWithDot - numberWithDot);

        int test1 = 10;
        double test2 = 3;
        double divideRes = test1 / test2;
        System.out.println(divideRes);

        double nullResInt = 10/0.0;//NOSONAR
        double nullResInt2 = 10/0d;

        System.out.println(nullResInt/nullResInt2);

        int multiplyResult = 10*3;
        System.out.println(multiplyResult);
        long longValue = 100_000_000_000l;
        int mod = 11 % 2;
        System.out.println(mod);

        System.out.println(minusResult);
        System.out.println(fromInt);
        System.out.println((char)(symbol1+symbol2));


        System.out.println(byteValue);
        System.out.println("first plus second =" + sum);



        boolean equals = 10 >= 7;
        System.out.println(equals);
    }
}
