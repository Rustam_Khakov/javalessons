package lesson13;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Rustam Khakov
 */
public class MapTest {

    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();
        map.put("Антон", "Лево-булачная 10");
        // put-> хеш код у "Антон" -> кладем его в корзину
        map.put("Анна", "Лево-булачная 12");
        // put-> хеш код у "Анна" -> кладем его в корзину
        System.out.println(map.get("Антон"));
        //1-> переопределить хешкод и еквалс у ключа
        //2-> ключ должен быть immutable

        Map<Key, String> address = new HashMap<>();
        Key key = new Key("Анита","Цой");
        address.put(key, "Moscow");
        System.out.println(address.get(key));
        System.out.println(address.get(key));

        Map<String, String> treeMap = new TreeMap<>();


    }
}
