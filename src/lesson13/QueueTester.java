package lesson13;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * @author Rustam Khakov
 */
public class QueueTester {

    public static void main(String[] args) {
        Queue<String> queue = new LinkedList<>();
        queue.add("Первый клиент");
        queue.add("Второй клиент");
        queue.add("Третий клиент");
        System.out.println(queue.remove());//Первый клиент удален -> если очередь пустая выкини ошибку
        queue.element();// peek но выкидывает ошибку если пустой
        System.out.println(queue.peek());//смотрит кто первый в очереди
        System.out.println(queue.poll());//достает первого в очереди
        queue.size();

        Stack<String> stack = new Stack<>();
        stack.push("Value1");//кладет значение
        stack.push("Value2");//кладет значение
        stack.push("Value3");//кладет значение
        stack.push("Value4");//кладет значение
        stack.peek();// посмотри начало стека
        System.out.println(stack.pop());//достет елемент
        stack.empty();//пустой или нет

    }
}
