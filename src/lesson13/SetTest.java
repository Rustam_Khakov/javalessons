package lesson13;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;
import java.util.TimeZone;

/**
 * @author Rustam Khakov
 */
public class SetTest {

    public static void main(String[] args) {
        Set<String> uniqueValues = new HashSet<>();
        uniqueValues.add("Anton");
        uniqueValues.add("Anton");
        uniqueValues.add("Anton");
        uniqueValues.add("Anton");
        uniqueValues.add("Anton");
        uniqueValues.add("Anna");
        uniqueValues.add("Sergey");
        System.out.println(uniqueValues.size());
        for (String uniqueValue : uniqueValues) {
            System.out.println(uniqueValue);
        }

        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        System.out.println(calendar.get(Calendar.DAY_OF_WEEK));

    }
}
