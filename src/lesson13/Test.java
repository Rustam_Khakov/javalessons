package lesson13;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Rustam Khakov
 */
public class Test {

    public static void main(String[] args) {
        Integer[] array = new Integer[5];
        array[0] = 1;
        array[1] = 2;
        array[2] = 3;
        array[3] = 4;
        array[4] = 5;
        Iterator<Integer> iterator = new ArrayIterator<>(array);
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        List<String> list = new ArrayList<>();
        List<String> list2 = new LinkedList<>();

    }
}
