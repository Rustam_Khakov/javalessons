package lesson13;

import java.util.Objects;

/**
 * @author Rustam Khakov
 */
public final class Key {
    private final String param1;
    private final String param2;

    public Key(String param1, String param2) {
        this.param1 = param1;
        this.param2 = param2;
    }

    public String getParam1() {
        return param1;
    }

    public String getParam2() {
        return param2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Key key = (Key) o;
        return Objects.equals(param1, key.param1) && Objects.equals(param2, key.param2);
    }

    @Override
    public int hashCode() {
        return Objects.hash(param1, param2);
    }
}
