package lesson13;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * @author Rustam Khakov
 */
public class MyLinkedList<T> implements List<T> {
    private Node<T> head;
    private Node<T> current;
    private int size;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        Node<T> node = head;
        while (node != null) {
            if (node.getVal() != null && node.getVal().equals(o)) {
                return true;
            }
            node = node.getNext();
        }
        return false;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean add(T t) {
        if (current != null) {
            Node<T> node = new Node<>(t);
            current.setNext(node);
            this.current = node;
            size++;
        } else {
            head = new Node<>(t);
            current = head;
        }
        return true;
    }

    @Override
    public boolean remove(Object o) {
        Node<T> node = head;
        Node<T> prev = null;
        while (node != null) {
            if (node.getVal() != null && node.getVal().equals(o)) {
                if (prev == null) {
                    head = head.getNext();
                } else {
                    prev.setNext(node.getNext());
                }
                size--;
                return true;
            }
            prev = node;
            node = node.getNext();
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {
        size = 0;
        head = null;
    }

    @Override
    public T get(int index) {
        int curr = 0;
        Node<T> node = head;
        while (node != null) {
            if (curr == index) {
                return node.getVal();
            }
            curr++;
            node = node.getNext();
        }
        return null;
    }

    @Override
    public T set(int index, T element) {
        return null;
    }

    @Override
    public void add(int index, T element) {

    }

    @Override
    public T remove(int index) {
        return null;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    public static class MyListIterator<T> implements Iterator<T> {
        Node<T> curr;

        public MyListIterator(MyLinkedList<T> list) {
            curr = list.head;
        }

        @Override
        public boolean hasNext() {
            return curr != null;
        }

        @Override
        public T next() {
            Node<T> val = curr;
            curr = curr.getNext();
            return val.getVal();
        }
    }
}
