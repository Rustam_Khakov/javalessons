package lesson13;

import java.util.Iterator;

/**
 * @author Rustam Khakov
 */
public class ArrayIterator<T> implements Iterator<T> {
    T[] array;
    int current;
    int length;

    public ArrayIterator(T[] array) {
        this.array = array;
        length = array.length;
    }

    @Override
    public boolean hasNext() {
        return current < length;
    }

    @Override
    public T next() {
        T val = array[current];
        current++;
        return val;
    }
}
