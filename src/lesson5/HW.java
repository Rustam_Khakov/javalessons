package lesson5;

import java.util.Random;

/**
 * @author Rustam Khakov
 */
public class HW {
    public static void main(String[] args) {
        //Вывести на консоль количество отрицательных элементов под главной диагональю матрицы
        int[][] array = new int[4][4];
        int count = 0;
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                int randomizedVal = random.nextInt(-25);
                int actual = randomizedVal - 25;
                array[i][j] = actual;
                if(i>=j && array[i][j]<0) {
                    count++;
                }
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println(count);
    }
}
