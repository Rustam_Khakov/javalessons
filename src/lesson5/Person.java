package lesson5;

/**
 * @author Rustam Khakov
 */
public class Person {
    int age;
    static String personType = "Человек";

    public Person() {
        System.out.println("Я родился");
    }

    public Person(int someAge){
        this.age = someAge;
    }
    public void jump(int meter){
        System.out.println("я прыгнул на %s метров".formatted(meter) );
    }

    public static void sayHello(){
        System.out.println("Hello I am a human");
    }
}
