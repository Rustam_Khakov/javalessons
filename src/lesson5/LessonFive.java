package lesson5;

import java.util.Scanner;

/**
 * @author Rustam Khakov
 */
public class LessonFive {
    public static void main(String[] args) {
        String value = new String("fsfsa");
        Person person = new Person();// где-то в памяти создали обхект и
        Person personTwo = person;//смотрит туда же
        Person personThree = new Person(4);
        System.out.println(person.age);
        System.out.println(personThree.age);
        person.jump(3);
        Person.sayHello();
        int res = MathOperations.sum(2,3);//5
        String resStr = String.valueOf(res); // "5"
        Integer integer = Integer.valueOf(resStr);//5
        System.out.println(integer);

        String str = "a@b";
        System.out.println(str.charAt(0));
        String result = str+ "va";
        System.out.println(result);
        String join = String.join(",", "D", "B", "C");
        String[] splitRes = join.split(",");
        for (int i = 0; i < splitRes.length; i++) {
            System.out.println(splitRes[i]);
        }
        System.out.println(join);
        String replace = "test".replace('e', 'a');
        System.out.println(replace);
        "VAV".equalsIgnoreCase("vAv");

    }
}