package lesson5;

/**
 * @author Rustam Khakov
 */
public class HW2 {
    public static void main(String[] args) {
        String s = "арозаупаланалапуазора";
        int length = s.length();
        for (int i = 0; i < length / 2; i++) {
            if (s.charAt(i) != s.charAt(length - 1 - i)) {
                System.out.println("не палиндром");
                break;
            }
        }
        String str = "Мама, Папа, Бабушка, Дед, Дочь, Сын";
        String[] res = str.split(", ");
        for (int i = 0; i < res.length; i++) {
            System.out.println(res[i]);
        }

        String first = "3";
        String sec= "4";
        Integer firstInt = Integer.valueOf(first);
        Integer secInt = Integer.valueOf(sec);
        System.out.println(firstInt + secInt);
        System.out.println(first + sec);
    }

}
