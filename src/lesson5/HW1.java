package lesson5;

import java.util.Scanner;

/**
 * @author Rustam Khakov
 */
public class HW1 {
    public static void main(String[] args) {
        //Ввести n строк с консоли, найти самую короткую и самую длинную строки. Вывести найденные строки и их длину.
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String min = null;
        String max = "";
        for (int i = 0; i <= n; i++) {
            String first = scanner.next();
            if (min == null) {
                min = first;
            }
            if(first.length()> max.length()) {
                max = first;
            }
            if(min.length()> first.length()) {
                min = first;
            }
        }
        System.out.println("Max str:" + max + " length: " + max.length());
        System.out.println("Min str:" + min + " length: " + min.length());


    }
}
