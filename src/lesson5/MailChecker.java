package lesson5;

/**
 * @author Rustam Khakov
 */
public class MailChecker {
    public static void main(String[] args) {
        String mail = "test@test.com";
        // нет пробела
        char[] restrictedChars = {'/',' ','"'};
        String[] vals = mail.split("@");
        boolean stratsWith = mail.startsWith("@");
        boolean endsWith = mail.endsWith("@");
        boolean hasSpace = mail.contains(" ");
        boolean hasRestricted = false;
        for (int i = 0; i < restrictedChars.length; i++) {
            if(mail.indexOf(restrictedChars[i]) != -1) {
                hasRestricted = true;
            }
        }
        if (vals.length == 2
                && !stratsWith
                && !endsWith
                && !hasSpace
                && hasRestricted
        ) {
            System.out.println("It is correct mail");
        } else {
            System.out.println("Incorrect mail");
        }

        //проверка на палиндром
        // арозаупаланалапуазора
        String val = "fdfsa";
        char[] chars = val.toCharArray();
        for (int i = 0; i <chars.length/2; i++) {
            // первый и последний индекс
            // i  последний - n-1-i
        }
    }
}
