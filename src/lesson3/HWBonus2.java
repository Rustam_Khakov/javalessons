package lesson3;

import java.util.Scanner;

/**
 * @author Rustam Khakov
 */
public class HWBonus2 {
    public static void main(String[] args) {
        //Bonus 2
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt(); // 4
        int[] array = new int[n];
        int result = 0;
        for(int i = 0; i< array.length; i++){
            array[i] = scanner.nextInt();
            if (array[i] %4 == 0 && array[i]% 7 != 0) {
                result++;
            }
        }
        System.out.println(result);
    }
}
