package lesson3;

import java.util.Scanner;

/**
 * @author Rustam Khakov
 */
public class HwBonus {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        int even = 0;
        for(int i = 0; i< array.length; i++){
            array[i] = scanner.nextInt();
            if (array[i] %2 == 0) {
                even++;
            }
        }
        int[] evenArray = new int[even];
        int j = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0) {
                evenArray[j] = array[i];
                j++;
            }
        }


    }
}
