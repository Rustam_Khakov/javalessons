package lesson3;

/**
 * @author Rustam Khakov
 */
public class HW3 {
    public static void main(String[] args) {//NOSONAR
        //i++; // сначала берем значение и потом увел
        //++i;//сначала увелич потом достаем
        //task 1
        //Создайте массив из 20 элементов и заполните их числами от 10 до 30 (используйте цикл)
        int[] arrayOne = new int[20];
        for (int i = 0; i < arrayOne.length; i++) {
            arrayOne[i] = i + 10;
            System.out.println(arrayOne[i]);
        }

        //Task 2
        //Есть два массива char[] first = {‘a'',''b'',''c'',''d'',''e''} и char[] second = {‘f'',''g'',''h'',''i'',''g''} создать массив соединяющий два этих массива в один новый
        char[] first = {'a', 'b', 'c', 'd', 'e'};
        char[] second = {'f', 'g', 'h', 'i', 'g'};
        char[] third = new char[first.length + second.length];
        for (int i = 0; i < third.length; i++) {
            if (i < first.length) {
                third[i] = first[i];
            } else {
                third[i] = second[i - first.length];
            }
        }

        //Task 3
        int[] four = {2, 2, 2, 2, 2, 2, 7, 8, 9, 0};
        for (int i = 0; i < four.length; i++) {
            if (four[i] % 2 == 0) {
                four[i] = 0;
            }
            System.out.println(four[i]);
        }
        //Task 4
        int max = four[0];
        for (int i = 0; i < four.length; i++) {
            if (four[i] > max) {
                max = four[i];
            }
        }
        //Task 5
        int sum = 0;
        int odd = 0;
        int even = 0;
        for (int i = 0; i < four.length; i++) {
            System.out.println(four[i]);
            System.out.println(four[four.length - 1 - i]);
            sum += four[i];
            if (four[i]% 2 == 0) {
                even++;
            } else {
                odd++;
            }
        }
        if (odd> even) {
            System.out.println("нечетных больше");
        }else if(odd == even) {
            System.out.println("ничья");
        } else {
            System.out.println("четных больше");
        }

        for (int i = four.length - 1; i >= 0; i--) {
            System.out.println(four[i]);
        }


    }
}
