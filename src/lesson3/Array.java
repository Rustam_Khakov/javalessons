package lesson3;

import java.util.Scanner;

/**
 * @author Rustam Khakov
 */
public class Array {
    public static void main(String[] args) {
        int[] arrayOne = new int[5];
        arrayOne[0] = 4;
        arrayOne[1] = 3;
        arrayOne[2] = 2;

        for (int i = 0; i < arrayOne.length; i++) {
            //System.out.println(arrayOne[i]);
        }

        int[] arrayTwo = {1, 2, 3, 5, 7};

        char[] dictionary = {'a', 'b', 'c'}; // length  is 3
        char[] dictionaryTwo = {'b', 'c', 'g', 'a'}; // length is 4
        char[] dictionaryThree = new char[dictionary.length + dictionaryTwo.length];

//        for (int i = 0; i < dictionaryThree.length; i++) {
//            if (i<dictionary.length) {
//                dictionaryThree[i] = dictionary[i];
//            } else {
//                dictionaryThree[i] = dictionaryTwo[i - dictionary.length];
//            }
//            System.out.println(dictionaryThree[i]);
//        }

        for (int i = 0; i < dictionaryTwo.length; i++) {
            for (int j = 0; j < dictionary.length; j++) {
                if (dictionaryTwo[i] == dictionary[j]) {
                    System.out.println(dictionaryTwo[i]);
                    break;
                }
            }
        }
        int length = 5;
        Scanner scanner = new Scanner(System.in);
        System.out.println("какой длины нужен массив?");
        length = scanner.nextInt();//считали длину
        int[] arrayThree = new int[length];//создали массив

        System.out.println("введите значения массива: ");//написали в консоль
        for (int i = 0; i < arrayThree.length; i++) {// начлаи заполнять массив
            arrayThree[i] = scanner.nextInt();// scanner.nextChar();
        }
        System.out.printf("результат массива длины %d \n", length);


        for (int i = 0; i<arrayThree.length;i++){
            System.out.println(arrayThree[i]);
        }
        //int i = 9;
        //char val = 'a';
        //char[] charArray = {'s','o','m',e',' ','s','t','r','i','n','g'}
        //charArray[0] ('s')
        String testString = "some string";
        testString.length();
        testString.charAt(0);


    }
}
