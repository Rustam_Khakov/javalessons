package lesson21;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Rustam Khakov
 */
public class LogTest {
    private static final Logger logger = LoggerFactory.getLogger(LogTest.class);

    public static void main(String[] args) {
        testFunction(2, 0);
        logger.trace("Hello trace");//
        logger.debug("Hello debug");
        logger.info("Hello");
        logger.warn("Hello warn");
        logger.error("Hello error");
    }

    private static void testFunction(int i, int i1) {
        try {
            if (i == 0 || i1 == 0) {
                throw new RuntimeException("неправильный пароль");
            }
        } catch (Exception e) {
            logger.error("Программа сломалась на данных: {} и {} ", i, i1, e);
        }
    }
}
