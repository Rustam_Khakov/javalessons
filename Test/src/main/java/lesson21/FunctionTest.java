package lesson21;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Rustam Khakov
 */
public class FunctionTest {
    public static void main(String[] args) {
        Func func = (first, sec) -> System.out.println(first + sec);
        func.apply(1, 2);
        func.apply(3, 4);
        List<String> list = Arrays.asList("first", "second", "third", "fourth", "first");

        //aaa
        //aaaa
        //aaab
        //ab

        Map<Integer, List<String>> collect = list
                .stream()
                .parallel()
                //.filter(val -> val.length() > 5)
                //.map(val -> val + " test")
                //.sorted(String::compareTo)
                .collect(Collectors.groupingBy(String::length));
        Stream<String> stream = list.stream();
        //boolean b = stream.map(String::length).noneMatch(val -> val > 5);
        List<String> collect1 = stream
                .skip(2)
                .limit(2)
                .collect(Collectors.toList());
        //terminal operations: collect, reduce, forEach, max, min

        System.out.println(collect);
        System.out.println(collect1);
    }
}
