package lesson19;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;


/**
 * @author Rustam Khakov
 */
public class SaxTest {
    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, JAXBException {
//        InputStream resourceStream = JsonTest.class.getResourceAsStream("/people.xml");
        InputStream resourceStream2 = JsonTest.class.getResourceAsStream("/people.xml");

//        SAXParserFactory factory = SAXParserFactory.newInstance();
//        SAXParser saxParser = factory.newSAXParser();
//        saxParser.parse(resourceStream, new Handler());

        DocumentBuilderFactory factory1 = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = factory1.newDocumentBuilder();
        Document document = documentBuilder.parse(resourceStream2);
        Element rootElement = document.getDocumentElement();
        System.out.println(rootElement.getElementsByTagName("firstName").item(0).getTextContent());
        JAXBContext context = JAXBContext.newInstance(People.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        JAXBElement<People> unmarshal = unmarshaller.unmarshal(document, People.class);
        System.out.println(unmarshal.getValue());
    }

    public static class Handler extends DefaultHandler {
        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            System.out.println(localName + " : " + qName);
        }
    }
}
