package lesson19;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Rustam Khakov
 */
public class Result {
    @SerializedName("results")
    List<People> peoples;

    public List<People> getPeoples() {
        return peoples;
    }

    public void setPeoples(List<People> peoples) {
        this.peoples = peoples;
    }
}
