package lesson19;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Rustam Khakov
 */
public class DateAdapter implements JsonSerializer<Date>, JsonDeserializer<Date> {
    private DateFormat format = new SimpleDateFormat("yyyy@MM@dd");

    @Override
    public JsonElement serialize(Date date, Type type,
                                 JsonSerializationContext jsonSerializationContext) {
        return new JsonPrimitive(format.format(date));
    }

    @Override
    public Date deserialize(JsonElement jsonElement, Type type,
                            JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        try {
            return format.parse(jsonElement.getAsString());
        } catch (ParseException e) {
            return null;
        }
    }
}
