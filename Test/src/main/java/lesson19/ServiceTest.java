package lesson19;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.function.Function;

/**
 * @author Rustam Khakov
 */
public class ServiceTest {
    public static void main(String[] args) throws IOException, InterruptedException {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create("https://randomuser.me/api"))
                .build();
//        while (true) {
//            HttpResponse<String> result = client.send(request, HttpResponse.BodyHandlers.ofString());
//            Gson gson = new GsonBuilder().create();
//            System.out.println(gson.fromJson(result.body(), Result.class).getPeoples());
//        }
        int val = 0;
        Function<Integer, Integer> function = x -> x + 5; // f(x) = x+5
        Function<Integer, Integer> func2 = ServiceTest::calculateFor5;
        Integer result = function.apply(4);
        Integer result2 = function.apply(4);
    }

    private static int calculateFor5(int val) {
        return val + 5;
    }
}
