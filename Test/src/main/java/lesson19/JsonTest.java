package lesson19;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Date;

/**
 * @author Rustam Khakov
 */
public class JsonTest {
    public static void main(String[] args) throws IOException {
        URL resource = JsonTest.class.getResource("/people.json");
        InputStream resourceStream = JsonTest.class.getResourceAsStream("/people.json");

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Date.class, new DateAdapter())
                .create();

        People gsonPeople = gson.fromJson(new InputStreamReader(resourceStream), People.class);
        System.out.println("Gson people: " + gsonPeople);


//        People people = mapper.readValue(resource, People.class);
//        mapper.writeValue(new File("people2.json"),people);
//        System.out.println(people);
    }
}
