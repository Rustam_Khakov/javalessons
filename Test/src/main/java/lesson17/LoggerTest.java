package lesson17;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Rustam Khakov
 */
public class LoggerTest {
    private static final Logger logger = LoggerFactory.getLogger(LoggerTest.class);

    public static void main(String[] args) {
        logger.info("Привет всем");
        logger.error("fas");
    }

}
